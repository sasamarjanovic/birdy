//
//  BirdyApp.swift
//  Birdy
//
//  Created by student on 13.12.2023..
//

import SwiftUI

let IMAGE_URL = URL(string: "https://upload.wikimedia.org/wikipedia/commons/3/3b/Blue-footed-booby.jpg")!

@main
struct BirdyApp: App {

    
    @StateObject var tweetData = TweetData()
    @StateObject var userData = UserData()
    
    var body: some Scene {
        WindowGroup {
            TabView {
                ContentView()
                    .tabItem {
                        Label("Tweets", systemImage: "list.bullet.circle")
                    }
                SearchView()
                    .tabItem {
                        Label("Search", systemImage: "magnifyingglass.circle")
                    }
                ProfileView()
                    .tabItem {
                        Label("Profile", systemImage: "person.circle")
                    }
            }
            .environmentObject(tweetData)
            .environmentObject(userData)
            .task {
                //await tweetData.sendTweet(tweet: Tweet(username: "heda", content: "prvi", isFavorite: false))
                //await tweetData.sendTweet(tweet: Tweet(username: "heda", content: "drugi", isFavorite: false))
                //await tweetData.sendTweet(tweet: Tweet(username: "heda", content: "treci", isFavorite: false))
                //await tweetData.sendTweet(tweet: Tweet(username: "heda", content: "cet", isFavorite: false))
                await tweetData.fetchTweets()
            }
        }
    }
}

