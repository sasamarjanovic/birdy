//
//  ContentView.swift
//  Birdy
//
//  Created by student on 13.12.2023..
//

import SwiftUI


struct ContentView: View {
    
    @State var content: String = ""
    @State var isPresented: Bool = false
    @State var username: String = ""
    
    @EnvironmentObject var tweetData: TweetData
    @EnvironmentObject var userData: UserData
    
    var body: some View {
        VStack{
            HStack {
                Image(systemName: "bird")
                    .imageScale(.large)
                    .foregroundStyle(.pink)
                Text("Birdy")
                    .font(.title)
                    .foregroundStyle(.pink)
                Spacer()
                if username.isEmpty{
                    Button(action: {isPresented = true}){
                        Text("Log in")
                    }
                } else{
                    Button(action: {username = ""}){
                        Text("Log out")
                    }
                }
                
            }
            List($tweetData.tweets){ tweet in
                TweetRow(tweet: tweet)
            }
            .listStyle(.plain)
            Spacer()
            if !username.isEmpty{
                HStack{
                    TextField("Content", text: $content)
                    Button(action: {
                        Task {
                            let tweet = Tweet(username: username, content: content, isFavorite: false)
                            await tweetData.sendTweet(tweet: tweet)
                            userData.myTweetsIds.append(tweet.id)
                            //tweetData.tweets.append(tweet) bolja praksa od linije ispod
                            await tweetData.fetchTweets()
                        }
                    }){
                        Text("Tweet")
                    }
                    .disabled(content.isEmpty)
                }
            }
        }
        
        .padding()
        .sheet(isPresented: $isPresented) {
            LoginView(username: $username, isPresented: $isPresented)
        }
    }
}

