//
//  ProfileView.swift
//  Birdy
//
//  Created by student on 11.01.2024..
//

import SwiftUI

struct ProfileView: View {
    
    @EnvironmentObject var tweetData: TweetData
    @EnvironmentObject var userData: UserData
    
    @State var image: UIImage? = nil
    
    var body: some View {
        VStack{
            HStack{
                
                if let newimage = image {
                    Image(uiImage: newimage)
                        .resizable()
                        .frame(width: 60, height: 60)
                        .clipShape(Circle())
                }
                else {
                    Circle()
                        .frame(width:55, height: 55)
                        .foregroundColor(.gray)
                }
                
                Text(userData.username)
                    .font(.subheadline)
                Spacer()
            }
            .padding()
            
            Text("My tweets")
            List(Binding.constant(tweetData.getTweets(inds:userData.myTweetsIds))){ tweet in
                TweetRow(tweet: tweet)
            }
            .listStyle(.plain)
            .padding()
            
            Text("Liked tweets")
            List(Binding.constant(tweetData.getTweets(inds: userData.likedTweetsIds))){ tweet in
                TweetRow(tweet: tweet)
            }
            .listStyle(.plain)
            .padding()

            Spacer()
        }.task {
            do {
                let (data, _) =  try await URLSession.shared.data(from: userData.imageUrl)
                image = UIImage(data: data)
            } catch let error {
                print(error)
            }
        }
    }

}


