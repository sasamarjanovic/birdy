//
//  TweetData.swift
//  Birdy
//
//  Created by student on 11.01.2024..
//

import Foundation


class TweetData: ObservableObject {
    
    let tweets_url = URL(string: "https://birdy-a50cd-default-rtdb.europe-west1.firebasedatabase.app/tweets.json")!
    
    @Published var tweets: [Tweet] = []
    
    func getTweets(inds: [String]) -> [Tweet]{
        return tweets.filter { tweet in
            return inds.contains(tweet.id)
        }
    }
    
    
    func sendTweet(tweet: Tweet) async
    {
        do {
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .iso8601
            let json = try encoder.encode(tweet)
            
            
            var request = URLRequest(url: tweets_url)
            request.httpMethod = "POST"
            request.httpBody = json
            
            let (_, response) = try await URLSession.shared.data(for: request)
            print(response)
        }catch let error {
            print(error)
        }
    }
    
    func fetchTweets() async
    {
        do {
            let (data, _) = try await URLSession.shared.data(from: tweets_url)
            
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            
            let decoded_tweets = try decoder.decode([String: Tweet].self, from: data)
            tweets = [Tweet](decoded_tweets.values)
        } catch let error {
            print(error)
        }
    }
}

