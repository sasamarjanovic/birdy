//
//  UserData.swift
//  Birdy
//
//  Created by student on 11.01.2024..
//

import Foundation
import Combine

class UserData: ObservableObject {
    @Published var username = "Sale"
    @Published var imageUrl = IMAGE_URL
    @Published var myTweetsIds: [String] = []
    @Published var likedTweetsIds: [String] = []
}

